package com.augmentum.book.controller;

import com.augmentum.book.service.BookSpiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;

import com.tencentcloudapi.captcha.v20190722.CaptchaClient;

import com.tencentcloudapi.captcha.v20190722.models.DescribeCaptchaResultRequest;
import com.tencentcloudapi.captcha.v20190722.models.DescribeCaptchaResultResponse;
@RestController
public class BookSpiterController {
    @Autowired
    private BookSpiterService bookSpiterService;

    @GetMapping("bookSpiter")
    public String getData(){
        bookSpiterService.process();
        return "SUCCESS";
    }

    @GetMapping("setIndexUrl")
    public String setIndexUrl(){
        bookSpiterService.setIndexUrl();
        return "SUCCESS";
    }
    @GetMapping("setIndexNextUrl")
    public String setIndexNextUrl(){
        bookSpiterService.setIndexNextUrl();
        return "SUCCESS";
    }
}
