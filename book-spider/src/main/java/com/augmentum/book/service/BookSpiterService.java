package com.augmentum.book.service;

import org.springframework.scheduling.annotation.Async;

public interface BookSpiterService {

    @Async
    public void process();

    @Async
    public void process1();

    public void setIndexUrl();

    public void setIndexNextUrl();
}
