package com.augmentum.book.dao;

import com.augmentum.book.bean.BookSpiter;
import com.augmentum.book.bean.Urls;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
@Repository
public interface BookSpiterMapper {

    int insertBatch(@Param("list") List<BookSpiter> list);

    int insertUrlBatch(@Param("list") List<Urls> list);

    int selectCount();

    Urls selectById(int id);
}
