package com.augmentum.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SptileBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(SptileBookApplication.class, args);
    }

}
