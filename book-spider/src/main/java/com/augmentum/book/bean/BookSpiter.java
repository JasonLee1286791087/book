package com.augmentum.book.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BookSpiter {

    private static final long serialVersionUID = 1L;

    private String isbn;

    private String name;

    private String author;

    private String description;

    private String image;

}
