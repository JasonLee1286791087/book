package com.augmentum.book.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Urls {

    private static final long serialVersionUID = 1L;

    private String url;

    private String type;
}
