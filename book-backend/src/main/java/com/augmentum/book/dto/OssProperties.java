package com.augmentum.book.dto;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 阿里云oss配置信息读取
 */
@Component
public class OssProperties implements InitializingBean {
    @Value("${aliyun.oss.access-id}")
    private String accessId;
    @Value("${aliyun.oss.access-key}")
    private String accessKey;
    @Value("${aliyun.oss.bucket}")
    private String bucket;
    @Value("${aliyun.oss.endpoint}")
    private String endpoint;
    @Value("${aliyun.oss.dir}")
    private String dir;

    public static String END_POINT;
    public static String ACCESS_ID;
    public static String ACCESS_KEY;
    public static String BUCKET;
    public static String DIR;

    @Override
    public void afterPropertiesSet() throws Exception {
        END_POINT = endpoint;
        ACCESS_ID = accessId;
        ACCESS_KEY = accessKey;
        BUCKET = bucket;
        DIR = dir;
    }
}
