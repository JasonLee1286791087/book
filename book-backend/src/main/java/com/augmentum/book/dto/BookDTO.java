package com.augmentum.book.dto;

import lombok.Data;

/**
 * 书籍添加信息转换类
 */
@Data
public class BookDTO {
    private String bookName;
    private String imgUrl;
    private String description;
    private String type;
    private String suitableCrowd;
}
