package com.augmentum.book.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 微信返回结果
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CodeToSessionResult {

    private int errcode;

    private String errmsg;

    private String session_key;

    private String openid;
}
