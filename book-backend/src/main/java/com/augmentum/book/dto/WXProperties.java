package com.augmentum.book.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "wx.code")
public class WXProperties {
    private String url;
    private String appid;
    private String secret;
    private String grant_type;

}
