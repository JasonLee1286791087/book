package com.augmentum.book.config;

import cn.hutool.core.util.StrUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.cors.reactive.CorsUtils;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Configuration
public class CorsConfig {
    @Bean
    public WebFilter corsFilter() {
        return (ServerWebExchange ctx, WebFilterChain chain) -> {
            ServerHttpRequest request = ctx.getRequest();

            /**
             * WebSocket will do the CORS config by setting with setAllowedOrigins("*") in WebSocketConfig class
             */
            if (CorsUtils.isCorsRequest(request))
            {
                ServerHttpResponse response = ctx.getResponse();
                HttpHeaders requestHeaders = request.getHeaders();
                HttpHeaders headers = response.getHeaders();

                if(!StrUtil.startWith(request.getURI().getPath(),"/channel/websocket"))
                {

                    headers.add("Access-Control-Allow-Origin", requestHeaders.getOrigin());
                    headers.add("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
                    headers.add("Access-Control-Max-Age", "18000L");
                    headers.add("Access-Control-Allow-Headers", "istoken,Authorization,Content-Type,Accept,Origin,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,X-Requested-With");
                    headers.add("Access-Control-Expose-Headers", "*");
                    headers.add("Access-Control-Allow-Credentials", "true");
                }

                if(CorsUtils.isPreFlightRequest(request))
                {
                    response.setStatusCode(HttpStatus.OK);
                    return Mono.empty();
                }
            }

            return chain.filter(ctx);
        };
    }
}

