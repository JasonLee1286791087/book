package com.augmentum.book.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 数据库存储的书籍信息类（ISBN）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("book_spiter")
public class BookSpiter implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId
    private Long id;
    private String isbn;
    private String name;
    private String author;
    private String image;
    private String description;
    private LocalDateTime createdTime;
    private LocalDateTime updatedTime;

}
