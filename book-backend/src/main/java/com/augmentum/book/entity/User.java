package com.augmentum.book.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 客户端用户表
 * </p>
 * <p>
 * 注意主键ID  @TableId,没有命名为id/那么需要自己配置或者加注解
 *
 * @author randyyang
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户主键ID
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别
     */
    private String sex;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 电话号码
     */
    private String mobileNumber;

    /**
     * 头像
     */
    private String headImg;

    /**
     *
     */
    private String openid;

    /**
     * 
     */
    private String sessionKey;
}
