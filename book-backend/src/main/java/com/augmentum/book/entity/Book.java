package com.augmentum.book.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("book")
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    private String name;

    private String author;

    private String image;

    private String description;

    /**
     * 书籍借出标识 0未借出  1已借出
     */
    private Integer status;

    private Long typeId;

    /**
     * 删除书籍标识 0未删除 1已删除
     */
    private Integer isDeleted;

    /**
     * 添加至借出书籍标识 0可添加（未在我的借出） 1不可添加（已在我的借出）
     */
    private Integer isBorrow;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;

    private Long createdBy;

    private Long updatedBy;
}
