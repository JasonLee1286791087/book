package com.augmentum.book.mapper;

import com.augmentum.book.entity.Borrow;
import com.augmentum.book.vo.BookStatusVo;
import com.augmentum.book.vo.BookVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BorrowMapper extends BaseMapper<Borrow> {

    /**
     * 我的借入
     * @return
     */
    List<BookVo> getBorrowBooks(@Param("userId") Long userId);

    /**
     * 我的借出
     * @return
     */
    List<BookVo> getLentBooks(@Param("userId") Long userId);
}
