package com.augmentum.book.mapper;

import com.augmentum.book.entity.BookTabData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface BookTabDataMapper extends BaseMapper<BookTabData> {
}
