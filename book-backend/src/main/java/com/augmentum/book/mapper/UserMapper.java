package com.augmentum.book.mapper;

import com.augmentum.book.entity.User;
import com.augmentum.book.vo.BookStatusVo;
import com.augmentum.book.vo.BookVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {
//这里面有丰富的查询方法,自定义的也可以写在这里
}
