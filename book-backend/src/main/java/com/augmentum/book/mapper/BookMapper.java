package com.augmentum.book.mapper;

import com.augmentum.book.entity.Book;
import com.augmentum.book.vo.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface BookMapper extends BaseMapper<Book> {

    IPage<List<BookPageVo>> listByPage(Page page,
                                       @Param("searchKey") String searchKey,
                                       @Param("key1") Long key1,
                                       @Param("key2") Long key2
                                       );

    List<BookTypeVo> listBook();

    List<TabVo> listTab();

    IPage<List<BookPageVo>> getBookByTypeId(Page page, @Param("typeId") Long typeId);
}
