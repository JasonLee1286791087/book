package com.augmentum.book.mapper;

import com.augmentum.book.entity.BookSpiter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface BookSpiterMapper extends BaseMapper<BookSpiter> {
}
