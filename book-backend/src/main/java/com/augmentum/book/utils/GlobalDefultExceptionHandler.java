package com.augmentum.book.utils;

import com.augmentum.book.exceptin.GlobalEnumException;
import com.augmentum.book.exceptin.GlobalException;
import com.augmentum.book.exceptin.WXException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局异常处理
 */
@Slf4j
@ControllerAdvice
public class GlobalDefultExceptionHandler {

    //声明要捕获的异常
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public <T> ResponseInformation defultExcepitonHandler(HttpServletRequest request, Exception e) {
        e.printStackTrace();
        if (e instanceof GlobalEnumException) {
            GlobalEnumException globalEnumException = (GlobalEnumException) e;
            return ResponseInformation.error(globalEnumException.getCode(), globalEnumException.getMessage());
        }
        if (e instanceof WXException) {
            WXException wxException = (WXException) e;
            return ResponseInformation.error(wxException.getCode(), wxException.getMessage());
        }
        if (e instanceof GlobalException) {
            GlobalException globalException = (GlobalException) e;
            return ResponseInformation.error(globalException.getCode(), globalException.getMessage());
        }
        //未知错误
        return ResponseInformation.failed(e.getMessage());
    }
}



