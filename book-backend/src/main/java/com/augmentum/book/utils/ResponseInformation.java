package com.augmentum.book.utils;

import com.augmentum.book.common.CommonConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 前台传输数据格式
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ResponseInformation<T> implements Serializable{
    private static final long serialVersionUID = 1L;

    private int code;
    private String msg;
    private T data;

    public static <T> ResponseInformation<T> ok() {
        return restResult(null, CommonConstants.SUCCESS, null);
    }

    public static <T> ResponseInformation<T> ok(T data) {
        return restResult(data, CommonConstants.SUCCESS, null);
    }

    public static <T> ResponseInformation<T> ok(T data, String msg) {
        return restResult(data, CommonConstants.SUCCESS, msg);
    }

    public static <T> ResponseInformation<T> failed() {
        return restResult(null, CommonConstants.FAIL, null);
    }

    public static <T> ResponseInformation<T> failed(String msg) {
        return restResult(null, CommonConstants.FAIL, msg);
    }

    public static <T> ResponseInformation<T> failed(T data) {
        return restResult(data, CommonConstants.FAIL, null);
    }

    public static <T> ResponseInformation<T> failed(T data, String msg) {
        return restResult(data, CommonConstants.FAIL, msg);
    }

    public static <T> ResponseInformation<T> error(int code, String msg) {
        return restResult(null, code, msg);
    }

    private static <T> ResponseInformation<T> restResult(T data, int code, String msg) {
        ResponseInformation<T> apiResult = new ResponseInformation<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }
}
