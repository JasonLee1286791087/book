package com.augmentum.book.utils;

import com.augmentum.book.dto.CodeToSessionResult;
import com.augmentum.book.dto.WXProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * 微信请求接口，根据code获得openid和sessionKey
 */
@Slf4j
@Component
public class WXCodeToSession {

    @Autowired
    private WXProperties wxProperties;

//    public static CodeToSessionResult getByCode(String code) {
//        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=wxf4ff430e34a562c0&secret=e7838750f18161c78928dc6fed509c1e&js_code="+ code +"&grant_type=authorization_code";
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().add(new WXMappingJackson2HttpMessageConverter());
//        return restTemplate.getForObject(url, CodeToSessionResult.class);
//    }

    public CodeToSessionResult getByCode(String code) {
        log.info("请求路径为" + wxProperties.getUrl());
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid={appid}&secret={secret}&js_code={code}&grant_type={grantType}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new WXMappingJackson2HttpMessageConverter());
        return restTemplate.getForObject(url, CodeToSessionResult.class, wxProperties.getAppid(), wxProperties.getSecret(), code, wxProperties.getGrant_type());
    }
}
