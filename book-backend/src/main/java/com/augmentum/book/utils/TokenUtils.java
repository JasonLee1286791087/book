package com.augmentum.book.utils;

import com.augmentum.book.entity.User;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

/**
 * token工具类
 */
public class TokenUtils {
    public static String getToken(User user) {
        String token = "";
        token = JWT.create().withAudience(user.getId().toString()).sign(Algorithm.HMAC256("123456789"));
        return token;
    }
}
