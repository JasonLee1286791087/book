package com.augmentum.book.utils;

import com.augmentum.book.entity.User;

/**
 *线程
 */
public class ThreadUtils<T> {

    private static final ThreadLocal<String> tokenHolder = new ThreadLocal<>();

    private static final ThreadLocal<String> userIDHolder = new ThreadLocal<>();

    private static final ThreadLocal<User> userHolder = new ThreadLocal<>();

    public static void setToken(String token){
        tokenHolder.set(token);
    }

    public static String getToken(){
        return tokenHolder.get();
    }

    public static void setUserId(String userId){
        userIDHolder.set(userId);
    }

    public static String getUserId(){
         return userIDHolder.get();
//        return "6";
    }

    public static void setUserHolder(User user){
        userHolder.set(user);
    }

    public static User getUserHolder(){
        return userHolder.get();
    }

    public static void remove(){
        tokenHolder.remove();
        userHolder.remove();
        userIDHolder.remove();
    }
}
