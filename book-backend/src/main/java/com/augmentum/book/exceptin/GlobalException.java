package com.augmentum.book.exceptin;

public class GlobalException extends RuntimeException{
    private int code;
    private String message;

    public GlobalException() {
    }

    public GlobalException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public GlobalException setCode(int code) {
        this.code = code;
        return this;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public GlobalException setMessage(String message) {
        this.message = message;
        return this;
    }
}
