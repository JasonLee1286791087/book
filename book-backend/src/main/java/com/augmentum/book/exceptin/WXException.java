package com.augmentum.book.exceptin;

public class WXException extends RuntimeException {

    private int code;

    private String message;

    public WXException() {}

    public WXException(int code){
        switch (code){
            case -1:
                this.message = ExceptionEnum.WX_BUSY.getMessage();
                break;
            case 40029:
                this.message = ExceptionEnum.WX_ERROR.getMessage();
                break;
            case 45011:
                this.message = ExceptionEnum.WX_LIMIT.getMessage();
                break;
            default:
                break;
        }
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
    @Override
    public String getMessage() {
        return message;
    }
}
