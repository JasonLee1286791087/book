package com.augmentum.book.exceptin;

public enum ExceptionEnum {

    WX_BUSY(-1, "系统繁忙，此时请开发者稍候再试"),
    WX_ERROR(40029, "code无效"),
    WX_LIMIT(45011, "频率限制，每个用户每分钟100次"),
    TOKEN_NO_EXITS(1001, "token不存在"),
    USER_NO_EXITS(1002, "用户不存在"),
    BOOK_NO_EXITS(1003, "书籍已借出，暂不可移除"),
    TOKEN_ERROR(1004, "token不正确");

    private int code;
    private String message;

    private ExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
