package com.augmentum.book.controller;

import cn.hutool.core.util.ObjectUtil;
import com.augmentum.book.service.BookService;
import com.augmentum.book.utils.ResponseInformation;
import com.augmentum.book.vo.BookDetailVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/book")
@AllArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping("/page")
    public ResponseInformation listBookByPage(Page page, String searchKey, Long key1, Long key2) {
        try {
            IPage listBook = bookService.listByPage(page, searchKey, key1, key2);

            return ResponseInformation.ok(listBook, "success");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage(), "failed");
        }
    }

    @GetMapping("/list")
    public ResponseInformation listBook() {
        try {
            return ResponseInformation.ok(bookService.listBook(), "success");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage(), "failed");
        }
    }

    @GetMapping("/tab")
    public ResponseInformation listTab() {
        try {
            return ResponseInformation.ok(bookService.listTab(), "success");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage(), "failed");
        }
    }

    @GetMapping("/{id}")
    public ResponseInformation getBookById(@PathVariable Long id) {
        try {
            BookDetailVo bookDetailVo = bookService.getBookById(id);
            if (ObjectUtil.isNotNull(bookDetailVo.getId())) {
                return ResponseInformation.ok(bookDetailVo, "success");
            } else {
                return ResponseInformation.ok(null, "Data does not exist");
            }
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage(), "failed");
        }
    }

    @GetMapping("/typeMore")
    public ResponseInformation getBookByTypeId(Page page, Long typeId) {
        try {
            return ResponseInformation.ok(bookService.getBookByTypeId(page, typeId), "success");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage(), "failed");
        }
    }
}
