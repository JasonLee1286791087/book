package com.augmentum.book.controller;

import com.augmentum.book.utils.ResponseInformation;
import com.tencentcloudapi.captcha.v20190722.CaptchaClient;
import com.tencentcloudapi.captcha.v20190722.models.DescribeCaptchaResultRequest;
import com.tencentcloudapi.captcha.v20190722.models.DescribeCaptchaResultResponse;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/book")
@AllArgsConstructor
public class TencentCaptchaController {
    @ResponseBody
    @PostMapping(value = "/LoginVf")
    public ResponseInformation<String> Tcaptcha( String ticket, String randstr) {
        try {
            Credential cred = new Credential("AKIDua3Un9CrKG6nNqvMGzWtzpOhkW1GuCOI", "unOpCJAl7VeW7RVKZYPzILCm8E5oGSMz");

            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("captcha.tencentcloudapi.com");

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);

            CaptchaClient client = new CaptchaClient(cred, "", clientProfile);

            String params = "{\"CaptchaType\":9,\"Ticket\":\"t02CnxxPtG-nQu1Nw5fL40a4a8xfj-uXYhHN2T3396np9B1dMf_BC3s4VOuOqQHtVq62YsJfbu3r4h5YsgjJt-kSP0W9jyr5xIjmKuuh-YE6QXH08cQUWDTvw**\",\"UserIp\":\"127.0.0.1\",\"Randstr\":\"@TRa\",\"CaptchaAppId\":2017295825,\"AppSecretKey\":\"0GZSYJflZtirbPLuaMUBu-Q**\"}";
            DescribeCaptchaResultRequest req = DescribeCaptchaResultRequest.fromJsonString(params, DescribeCaptchaResultRequest.class);

            DescribeCaptchaResultResponse resp = client.DescribeCaptchaResult(req);

            if (resp.getCaptchaCode() == 1) {
                return ResponseInformation.ok(resp.getCaptchaMsg(), "success");
            }
            return ResponseInformation.failed(resp.getCaptchaMsg(), "failed");

        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage(), "failed");
        }
    }
}
