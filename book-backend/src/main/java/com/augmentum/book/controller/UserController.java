package com.augmentum.book.controller;

import com.augmentum.book.config.UserLoginToken;
import com.augmentum.book.dto.BookDTO;
import com.augmentum.book.dto.WXProperties;
import com.augmentum.book.entity.User;
import com.augmentum.book.service.UserService;
import com.augmentum.book.utils.ResponseInformation;
import com.augmentum.book.utils.ThreadUtils;
import com.augmentum.book.utils.TokenUtils;
import com.augmentum.book.vo.BookSpiteVo;
import com.augmentum.book.vo.BookStatusVo;
import com.augmentum.book.vo.BookVo;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.ISBN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 客户端用户表 前端控制器
 * </p>
 *
 * @author zhangxiaoxiang
 * @since 2019-07-04
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/login")
    public ResponseInformation login(@RequestParam Map<String, String> params){
        try {
            User user = userService.login(params);
            log.info("用户为"+ user.toString());
            if (user != null) {
                Map<String, String> map = new HashMap<>();
                map.put("token", TokenUtils.getToken(user));
                return ResponseInformation.ok(map,"登陆成功");
            } else {
                return ResponseInformation.failed("登录失败");
            }
        } catch (Exception e) {
            log.info("异常为"+ e.getMessage());
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 我的书籍
     * @return
     */
    @UserLoginToken
    @RequestMapping("/myBooks")
    public ResponseInformation list(){
        try {
            String userId = ThreadUtils.getUserId();
            Map<String, List<BookStatusVo>> map = new HashMap<>();
            Map<Integer, List<BookStatusVo>> collect = userService.getBooks(userId).stream().collect(Collectors.groupingBy(BookStatusVo::getIsDeleted));
            collect.forEach((key, value)-> {
                if (key == 0) {
                    map.put("shelvedList", value);
                } else {
                    map.put("unShelvedList", value);
                }
            });
            return ResponseInformation.ok(map);
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 我的未上架至可借出的书籍
     * @return
     */
    @UserLoginToken
    @GetMapping("/myShelfBooks")
    public ResponseInformation shelfList(){
        try {
            String userId = ThreadUtils.getUserId();
            List<BookVo> list = userService.getShelfBooks(userId);
            return ResponseInformation.ok(list);
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 我的借入
     * @return
     */
    @UserLoginToken
    @GetMapping("/myBorrowBooks")
    public ResponseInformation borrowList(){
        try {
            String userId = ThreadUtils.getUserId();
            List<BookVo> list = userService.getBorrowBooks(userId);
            return ResponseInformation.ok(list);
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 我的借出
     * @return
     */
    @UserLoginToken
    @GetMapping("/myLentBooks")
    public ResponseInformation lentList(){
        try {
            String userId = ThreadUtils.getUserId();
            List<BookVo> list = userService.getLentBooks(userId);
            return ResponseInformation.ok(list,"查询借出书籍成功");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    @UserLoginToken
    @GetMapping("/queryBook")
    public ResponseInformation getBook(@RequestParam String ISBN){
        try {
            log.info("ISBN" +ISBN);
            BookSpiteVo bookSpiteVo = userService.getBook(ISBN);
            if (bookSpiteVo == null) {
                return ResponseInformation.ok(null,"书籍不存在,清手动添加");
            }
            return ResponseInformation.ok(bookSpiteVo,"查询书籍成功");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 借阅书籍
     * @param bookId
     * @return
     */
    @UserLoginToken
    @PutMapping("/borrow/{bookId}")
    public ResponseInformation borrowBook(@PathVariable String bookId){
        try {
            String userId = ThreadUtils.getUserId();
            userService.borrowBook(userId, bookId);
            return ResponseInformation.ok(null,"借书成功");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 归还书籍
     * @param bookId
     * @return
     */
    @UserLoginToken
    @PutMapping("/return/{bookId}")
    public ResponseInformation returnBook(@PathVariable String bookId){
        try {
            String userId = ThreadUtils.getUserId();
            userService.returnBook(userId, bookId);
            return ResponseInformation.ok(null,"还书成功");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 增加可借出书籍
     * @param bookId
     * @return
     */
    @UserLoginToken
    @PutMapping("/shelf/{bookId}")
    public ResponseInformation addLentBook(@PathVariable String bookId){
        try {
            userService.addLentBook(bookId);
            return ResponseInformation.ok(null,"增加可借出书籍成功");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 移除可借出书籍
     * @param bookId
     * @return
     */
    @UserLoginToken
    @PutMapping("/removeLent/{bookId}")
    public ResponseInformation removeLentBook(@PathVariable String bookId){
        try {
            userService.removeLentBook(bookId);
            return ResponseInformation.ok(null,"移除可借出书籍成功");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 移除书籍
     * @param
     * @return
     */
    @UserLoginToken
    @PutMapping("/remove/{bookId}")
    public ResponseInformation removeBook(@PathVariable String bookId){
        try {
            String userId = ThreadUtils.getUserId();
            log.info(" book  书籍"+bookId);
            userService.removeBook(userId, bookId);
            return ResponseInformation.ok(null,"书籍移除成功");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }

    /**
     * 添加书籍
     * @param bookDTO
     * @return
     */
    @UserLoginToken
    @PostMapping("/add")
    public ResponseInformation addBook(@RequestBody BookDTO bookDTO){
        try {
            String userId = ThreadUtils.getUserId();
            userService.addBook(userId, bookDTO);
            return ResponseInformation.ok(null,"书籍添加成功");
        } catch (Exception e) {
            return ResponseInformation.failed(e.getMessage());
        }
    }
}
