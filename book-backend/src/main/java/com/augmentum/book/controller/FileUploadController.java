package com.augmentum.book.controller;

import com.augmentum.book.utils.OssUtil;
import com.augmentum.book.utils.ResponseInformation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传控制类
 */
@Slf4j
@RestController
@RequestMapping("/support")
public class FileUploadController {

    /**
     * 上传图片
     * @param file
     * @return 图片路径url
     */
    @PostMapping("/uploadImg")
    public ResponseInformation uploadImg(@RequestParam("file") MultipartFile file) {
        OssUtil ossUtil = new OssUtil();
        return ResponseInformation.ok(ossUtil.checkImage(file));
    }
}
