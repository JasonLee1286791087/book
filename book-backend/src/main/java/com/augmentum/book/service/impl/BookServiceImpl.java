package com.augmentum.book.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.augmentum.book.entity.Book;
import com.augmentum.book.mapper.BookMapper;
import com.augmentum.book.service.BookService;
import com.augmentum.book.vo.BookDetailVo;
import com.augmentum.book.vo.BookPageVo;
import com.augmentum.book.vo.BookTypeVo;
import com.augmentum.book.vo.TabVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import java.util.List;

@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements BookService {

    @Override
    public IPage<List<BookPageVo>> listByPage(Page page, String searchKey, Long key1, Long key2) {
        IPage result = baseMapper.listByPage(page, searchKey, key1, key2);
        List<BookPageVo> bookList = result.getRecords();
        for (BookPageVo book : bookList) {
            if (ObjectUtil.isNotNull(book) && StrUtil.isNotBlank(book.getName())) {
                book.setName(HtmlUtils.htmlUnescape(book.getName()));
                book.setAuthor(HtmlUtils.htmlUnescape(book.getAuthor()));
                book.setDescription(HtmlUtils.htmlUnescape(book.getDescription()));
            }
        }

        return result;
    }

    @Override
    public List<BookTypeVo> listBook() { return baseMapper.listBook(); }

    @Override
    public List<TabVo> listTab() { return baseMapper.listTab(); }

    @Override
    public BookDetailVo getBookById(Long id) {
        Book book = this.getById(id);
        BookDetailVo bookDetailVo = BeanUtil.toBean(book, BookDetailVo.class);

        return bookDetailVo;
    }

    @Override
    public IPage<List<BookPageVo>> getBookByTypeId(Page page, Long typeId) { return baseMapper.getBookByTypeId(page, typeId); }
}
