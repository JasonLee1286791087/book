package com.augmentum.book.service;

import com.augmentum.book.dto.BookDTO;
import com.augmentum.book.entity.Book;
import com.augmentum.book.entity.User;
import com.augmentum.book.dto.UserDTO;
import com.augmentum.book.vo.BookSpiteVo;
import com.augmentum.book.vo.BookStatusVo;
import com.augmentum.book.vo.BookVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface UserService extends IService<User> {
    /**
     * 根据用户ID查询用户详情
     * @param id
     * @return
     */
    User getUser(String id);

    int createUser(User user);

    /**
     * 用户登录
     * @param params
     * @return User
     */
    User login(Map<String, String> params);

    /**
     * 我借书的书籍
     * @param userId
     * @return
     */
    List<BookVo> getBorrowBooks(String userId);

    /**
     * 我的可借出书籍
     * @param userId
     * @return
     */
    List<BookVo> getLentBooks(String userId);

    /**
     * 可上架至我的借出书籍
     * @param userId
     * @return
     */
    List<BookVo> getShelfBooks(String userId);

    /**
     * 我的书籍
     * @param userId
     * @return
     */
    List<BookStatusVo> getBooks(String userId);

    /**
     * 借书
     * @param userId
     * @param bookId
     */
    void borrowBook(String userId, String bookId);

    /**
     * 我的借阅书籍 归还
     * @param userId
     * @param bookId
     */
    void returnBook(String userId, String bookId);

    /**
     * 我的书籍移除
     * @param userId
     * @param bookId
     */
    void removeBook(String userId, String bookId);

    /**
     * 我的书籍添加
     * @param userId
     * @param bookDTO
     */
    void addBook(String userId, BookDTO bookDTO);
    /**
     * 我的借出书籍 添加
     * @param bookId
     */
    void addLentBook(String bookId);

    /**
     * 我的借出书籍 移除
     * @param bookId
     */
    void removeLentBook(String bookId);

    /**
     * 通过ISBN码查询数据库中存储的书籍信息
     * @param ISBN
     * @return
     */
    BookSpiteVo getBook(String ISBN);
}
