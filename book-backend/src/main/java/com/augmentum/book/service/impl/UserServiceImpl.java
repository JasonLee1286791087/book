package com.augmentum.book.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.augmentum.book.dto.BookDTO;
import com.augmentum.book.dto.CodeToSessionResult;
import com.augmentum.book.entity.*;
import com.augmentum.book.exceptin.ExceptionEnum;
import com.augmentum.book.exceptin.GlobalEnumException;
import com.augmentum.book.exceptin.WXException;
import com.augmentum.book.mapper.*;
import com.augmentum.book.utils.WXCodeToSession;
import com.augmentum.book.vo.BookSpiteVo;
import com.augmentum.book.vo.BookStatusVo;
import com.augmentum.book.vo.BookVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.augmentum.book.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 客户端用户表 服务实现类
 * </p>
 *
 * @author randyyang
 * @since 2019-07-04
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private BookMapper bookMapper;
    @Autowired
    private BorrowMapper borrowMap;
    @Autowired
    private BookTabDataMapper bookTabDataMapper;
    @Autowired
    private BookSpiterMapper bookSpiterMapper;
    @Autowired
    private WXCodeToSession wxCodeToSession;
    /**
     * 根据用户ID查询用户详情
     *
     * @param id
     * @return
     */
    @Override
    public User getUser( String id) {
        return baseMapper.selectById(id);
    }

    /**
     * 创建用户
     *
     * @param user
     * @return
     */
    @Override
    public int createUser(User user) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getId, user.getId());
        return baseMapper.update(user, queryWrapper);
    }

    @Override
    public User login(Map<String, String> params) {
        String code = params.get("code");
        log.info("code为"+ code);
        CodeToSessionResult result = wxCodeToSession.getByCode(code);
        log.info("微信返回结果为"+ result.toString());
        if (result.getErrcode() == 0) {
            QueryWrapper<User> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().eq(User::getOpenid, result.getOpenid());
            User user = this.getOne(queryWrapper);
            // 用户第一次登录
            if (user == null) {
                user = this.convertUserVoToUser(params);
                user.setOpenid(result.getOpenid());
                user.setSessionKey(result.getSession_key());
                baseMapper.insert(user);
            } else {
                user.setSessionKey(result.getSession_key());
                baseMapper.updateById(user);
            }
            return this.getOne(queryWrapper);
        } else {
            throw new WXException(result.getErrcode());
        }
    }

    private User convertUserVoToUser(Map<String, String> params) {
        User user = new User();
        user.setHeadImg(params.get("avatarUrl"));
        user.setName(params.get("nickName"));
        user.setNickName(params.get("nickName"));
        user.setSex(params.get("gender"));
        return user;
    }

    @Override
    public List<BookVo> getBorrowBooks(String userId) {
        log.info(userId + "借阅的书籍");
        return borrowMap.getBorrowBooks(Long.parseLong(userId));
    }

    @Override
    public List<BookVo> getLentBooks(String userId) {
        log.info(userId + "借出的书籍");
        return borrowMap.getLentBooks(Long.parseLong(userId));
    }

    /**
     * 可上架至我的借出书籍
     * @param userId
     * @return
     */
    @Override
    public List<BookVo> getShelfBooks(String userId) {
        log.info(userId + "的书籍");
        QueryWrapper<Book> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Book::getCreatedBy, userId).eq(Book::getIsBorrow, 0).eq(Book::getIsDeleted,0);
        List<BookVo> books = new ArrayList<>();
        for(Book book: bookMapper.selectList(queryWrapper)){
            BookVo bookVo = new BookVo();
            BeanUtil.copyProperties(book, bookVo);
            books.add(bookVo);
        }
        return books;
    }

    /**
     * 我的借出书籍 添加
     * @param bookId
     */
    @Override
    public void addLentBook(String bookId) {
        Book book = new Book();
        QueryWrapper<Book> bookQueryWrapper = new QueryWrapper<>();
        bookQueryWrapper.lambda().eq(Book::getId, bookId);
        book.setIsBorrow(1);
        bookMapper.update(book, bookQueryWrapper);
    }

    /**
     * 我的借出书籍 移除
     * @param bookId
     */
    @Override
    public void removeLentBook(String bookId) {
        Book book = bookMapper.selectById(bookId);
        if (book.getStatus() == 1) {
            throw new GlobalEnumException(ExceptionEnum.BOOK_NO_EXITS);
        }
        book.setIsBorrow(0);
        bookMapper.updateById(book);
    }

    @Override
    public List<BookStatusVo> getBooks(String userId) {
        log.info(userId + "的书籍");
        QueryWrapper<Book> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Book::getCreatedBy, userId);
        List<BookStatusVo> books = new ArrayList<>();
        for(Book book: bookMapper.selectList(queryWrapper)){
            BookStatusVo bookStatusVo = new BookStatusVo();
            BeanUtil.copyProperties(book, bookStatusVo);
            books.add(bookStatusVo);
        }
        return books;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void borrowBook(String userId, String bookId) {
        log.info(userId + "在借阅"+ bookId +"书籍");
//        Book book = new Book();
//        QueryWrapper<Book> bookQueryWrapper = new QueryWrapper<>();
//        bookQueryWrapper.lambda().eq(Book::getId, bookId);
        Book book = bookMapper.selectById(bookId);
        book.setStatus(1);
        bookMapper.updateById(book);
        Borrow borrow = new Borrow();
        borrow.setBorrowUserId(Long.parseLong(userId));
        borrow.setBookId(Long.parseLong(bookId));
        borrow.setOwnerId(book.getCreatedBy());
        borrow.setBorrowTime(LocalDateTime.now());
        borrow.setStatus(1);
        borrowMap.insert(borrow);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void returnBook(String userId, String bookId) {
        log.info(userId + "在归还"+ bookId +"书籍");
        Book book = new Book();
        QueryWrapper<Book> bookQueryWrapper = new QueryWrapper<>();
        bookQueryWrapper.lambda().eq(Book::getId, bookId);
        book.setStatus(0);
        bookMapper.update(book, bookQueryWrapper);
        QueryWrapper<Borrow> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Borrow::getBookId, bookId).eq(Borrow::getBorrowUserId, userId);
        Borrow borrow = new Borrow();
        borrow.setReturnTime(LocalDateTime.now());
        borrow.setStatus(0);
        borrowMap.update(borrow,queryWrapper);
    }

    @Override
    public void removeBook(String userId, String bookId) {
        log.info(userId + "在移除"+ bookId +"书籍");
        Book book = bookMapper.selectById(bookId);
        if (book.getStatus() == 1) {
            throw new GlobalEnumException(ExceptionEnum.BOOK_NO_EXITS);
        }
        book.setIsBorrow(0);
        book.setIsDeleted(1);
        bookMapper.updateById(book);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void addBook(String userId, BookDTO bookDTO) {
        log.info(userId + "在上架书籍");
        Book book = this.BookDTOConvertBook(userId, bookDTO);
        bookMapper.insert(book);
        Long bookId = book.getId();
        BookTabData bookTabData = new BookTabData();
        bookTabData.setBookId(bookId);
        bookTabData.setTabDataId(Long.parseLong(bookDTO.getType()));
        bookTabDataMapper.insert(bookTabData);
        BookTabData bookTabData1 = new BookTabData();
        bookTabData1.setBookId(bookId);
        bookTabData1.setTabDataId(Long.parseLong(bookDTO.getSuitableCrowd()));
        bookTabDataMapper.insert(bookTabData1);
    }

    @Override
    public BookSpiteVo getBook(String ISBN) {
        QueryWrapper<BookSpiter> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(BookSpiter::getIsbn, ISBN);
        BookSpiter bookSpiter = bookSpiterMapper.selectOne(queryWrapper);
        if (bookSpiter == null) {
            return null;
        }
        BookSpiteVo bookSpiteVo = new BookSpiteVo();
        BeanUtil.copyProperties(bookSpiter, bookSpiteVo);
        return bookSpiteVo;
    }


    private Book BookDTOConvertBook(String userId, BookDTO bookDTO){
        User user = baseMapper.selectById(Long.parseLong(userId));
        Book book = new Book();
        book.setAuthor(user.getNickName());
        book.setIsDeleted(0);
        book.setStatus(0);
        book.setTypeId((long) 1);
        book.setIsBorrow(0);
        book.setCreatedBy(Long.parseLong(userId));
        book.setCreatedTime(LocalDateTime.now());
        book.setDescription(bookDTO.getDescription());
        book.setImage(bookDTO.getImgUrl());
        book.setName(bookDTO.getBookName());
        return book;
    }
}
