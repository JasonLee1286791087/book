package com.augmentum.book.service;

import com.augmentum.book.entity.Book;
import com.augmentum.book.vo.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface BookService extends IService<Book> {

    /**
     * 根据搜索关键字，类型id分页查询
     * @param page
     * @param searchKey
     * @param key1
     * @param key2
     * @return
     */
    IPage<List<BookPageVo>> listByPage(Page page, String searchKey, Long key1, Long key2);

    /**
     * 根据首页推荐查询书籍
     * @param
     * @return
     */
    List<BookTypeVo> listBook();

    /**
     * 分类列表
     * @return
     */
    List<TabVo> listTab();

    /**
     * 根据id查询图书
     * @param id
     * @return
     */
    BookDetailVo getBookById(Long id);

    /**
     * 根据类型id得到图书
     * @param typeId
     * @return
     */
    IPage<List<BookPageVo>> getBookByTypeId(Page page, Long typeId);
}
