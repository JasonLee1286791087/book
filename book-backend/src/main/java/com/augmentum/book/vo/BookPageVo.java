package com.augmentum.book.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BookPageVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String author;

    private String description;

    private String image;
}
