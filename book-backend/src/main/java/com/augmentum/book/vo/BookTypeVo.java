package com.augmentum.book.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BookTypeVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String displayName;

    private List<BookVo> typeData;
}
