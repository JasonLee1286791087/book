package com.augmentum.book.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 书籍信息类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BookSpiteVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private String image;
    private String description;
}
