package com.augmentum.book.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 我的书籍状态类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BookStatusVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer isDeleted;

    private Long id;

    private String name;

    private String image;

    /**
     * 书籍借出标识 0未借出  1已借出
     */
    private Integer status;
}