package com.augmentum.book.vo;

import com.augmentum.book.entity.Book;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BookDetailVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String author;

    private String description;

    private Integer status;

    private Long createdBy;

    private String image;
}
